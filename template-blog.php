<?php
/*
Template Name: Blog
*/
get_header();
global $woo_options;
?>
    <!-- #content Starts -->
	<?php woo_content_before(); ?>
    <?php 
    $blog_title = __( 'The Blog', 'woothemes' );
    $blog_description = '';
    
    if ( isset( $woo_options['woo_blog_title'] ) && ( $woo_options['woo_blog_title'] != '' ) ) {
		$blog_title = $woo_options['woo_blog_title'];
	}
 	if ( isset( $woo_options['woo_blog_description'] ) && ( $woo_options['woo_blog_description'] != '' ) ) { 
		$blog_description = $woo_options['woo_blog_description'];
	 }
	?>   
					
	<div id="title-container" class="col-full post">
		<h2 class="title"><?php echo stripslashes( $blog_title ); ?></h2>
		<?php if ( $blog_description != '' )  { ?>
		<span class="blog-title-sep">&bull;</span><span class="description"><?php echo stripslashes( $blog_description ); ?></span>
		<?php } ?>
		<?php get_template_part( 'search', 'form' ); ?>
	</div>

    <div id="content" class="page col-full">

		<?php if ( isset( $woo_options['woo_breadcrumbs_show'] ) && $woo_options['woo_breadcrumbs_show'] == 'true' ) { ?>
		<div id="content-header">
		
			<div id="breadcrumbs">
				<?php woo_breadcrumbs(); ?>
			</div><!--/#breadcrumbs -->
				
	        <div class="fix"></div>
	        
    	</div><!-- #content-header   -->  	
		<?php } ?>  	

	    <div id="inner" class="col-full">
			           		
			<div id="main" class="col-left">
	
	        <?php
	        	if ( get_query_var( 'paged' ) ) { $paged = get_query_var( 'paged' ); } elseif ( get_query_var( 'page') ) { $paged = get_query_var( 'page' ); } else { $paged = 1; }
	        	
	        	query_posts( array( 'post_type' => 'post', 'paged' => $paged ) );
	        	
	        	if ( have_posts() ) { $count = 0; while ( have_posts() ) { the_post(); $count++; 
	                                                                    
	            			$ico_cal = $woo_options['woo_post_calendar'] == 'true';
							$full_content = $woo_options['woo_post_content'] != 'content';
						?>
							<!-- Post Starts -->
						    <div <?php post_class(); ?>>
						    
						    <?php if ( $full_content ) { if ( $ico_cal ) { ?>
						    <div class="ico-cal alignleft"><div class="ico-day"><?php the_time( 'd' ); ?></div><div class="ico-month"><?php the_time( 'M' ); ?></div></div>
						    <?php } else { woo_image( 'width=' . $woo_options['woo_thumb_w'] . '&height=' . $woo_options['woo_thumb_h'] . '&class=thumbnail ' . $woo_options['woo_thumb_align'] ); } } ?>
						    	
						    	<div class="details" <?php if ( $ico_cal && $full_content ) { echo 'style="margin-left:52px;"'; } ?>>
						    	
					        	<h2 class="title"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
					        	
					            <?php woo_post_meta(); ?>
					            
					            <div class="entry">
					            <?php global $more; $more = 0; ?>
	                    <?php if ( isset( $woo_options['woo_post_content'] ) && $woo_options['woo_post_content'] == 'content' ) { the_content( __( 'Read More...', 'woothemes' ) ); } else { the_excerpt(); } ?>
	                </div><!-- /.entry -->
	
	                <div class="post-more">      
	                	<?php if ( isset( $woo_options['woo_post_content'] ) && $woo_options['woo_post_content'] == 'excerpt' ) { ?>
	                    <span class="read-more"><a href="<?php the_permalink(); ?>" title="<?php esc_attr_e( 'Continue Reading &rarr;', 'woothemes' ); ?>"><?php _e( 'Continue Reading &rarr;', 'woothemes' ); ?></a></span>
	                    <?php } ?>
	                </div>
					           </div><!-- /.details -->
					        </div><!-- /.post -->
	                                                
	        <?php
	        		}
	        	} else {
	        ?>
	            <div <?php post_class(); ?>>
	                <p><?php _e( 'Sorry, no posts matched your criteria.', 'woothemes' ); ?></p>
	            </div><!-- /.post -->
	        <?php } ?>
	        <?php woo_pagenav(); ?>
			<?php wp_reset_query(); ?>                
	
	        </div><!-- /#main -->
	            
			<?php get_sidebar(); ?>
	
		</div><!-- /#inner -->
    </div><!-- /#content -->    
		
<?php get_footer(); ?>