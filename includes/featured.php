<?php
global $woo_options, $post;
$settings = array(
					'slider_entries' => 5, 
					'featured_slide_group' => '', 
					'slider_title' => 'true', 
					'slider_content' => 'true', 
					'slider_pagination' => 'true'
				);

$settings = woo_get_dynamic_values( $settings );
?>
<div id="slides">
	<div id="slide-box">
	<?php
		$args = array( 'suppress_filters' => 0, 'post_type' => 'slide', 'posts_per_page' => intval( $settings['slider_entries'] ) );
		if ( 0 < intval( $settings['featured_slide_group'] ) ) {
			$args['tax_query'] = array(
										array( 'taxonomy' => 'slide-page', 'field' => 'id', 'terms' => array( intval( $settings['featured_slide_group'] ) ) )
									);
		}

		$slides = new WP_Query( $args );
	?>
	<?php if ( $slides->have_posts() ) : ?>
		<div class="slides_container col-full">  
		<?php while ( $slides->have_posts() ) : $slides->the_post(); ?>
			
			<div class="slide" <?php if( 1 == $settings['slider_entries'] ) { echo 'style="display:block;"'; }?>>
			<?php
				$url = get_post_meta($post->ID, 'url', true);
				$slide_content_class = 'entry';
				$has_video = get_post_meta( $post->ID, 'embed', true );
				$has_image = get_post_meta( $post->ID, 'image', true );
				$post_thumb = get_option('woo_post_image_support') == 'true' && has_post_thumbnail();
				$title = $settings['slider_title'] == 'true';
				$content = $settings['slider_content'] == 'true';
			?>
			<?php if ( $has_video ) { $slide_content_class = 'vid-content'; } else { $slide_content_class = 'slide-content'; } ?>
			
			<?php if ( $has_video || $has_image || $post_thumb ) {	?>
			<?php if ( ( $title || $content ) OR $has_video ) { ?>
				<div class="entry <?php echo $slide_content_class; ?> fl">
				
					<?php if ( $title AND !$has_video ) { ?><h2 class="title"><a href="<?php if ( $url ) { echo $url; } else { echo '#'; } ?>"><?php the_title(); ?></a></h2><?php } ?>
					
					<?php if ( $content OR $has_video ) { the_content(); } ?>
				
				</div><!-- /.slide-content -->
				<?php } ?>
					
					<?php if ( $has_image || $post_thumb ) { ?>
					<div class="slide-image fl">
					<?php if ( $url ) { ?>
					<a href="<?php echo $url; ?>" title="<?php the_title(); ?>"><?php woo_image('key=image&width=920&height=338&class=slide-img&link=img'); ?></a>
						<?php } else { ?>
						<?php woo_image('key=image&width=920&height=338&class=slide-img&link=img'); } ?>
					</div><!-- /.slide-image -->
					
					<?php } elseif ( $has_video ) {
					
						echo woo_embed('key=embed&width=500&class=video');
						
					  } ?>
								
					<div class="fix"></div>
					
				<?php } else { ?><!-- // End $type IF Statement -->
                
                	<div class="entry">
	                    <?php the_content(); ?>
                    </div>                        
               
                <?php } ?>
	                
			</div><!-- /.slide -->
			
		<?php endwhile; ?>
		<?php wp_reset_postdata(); ?>	
		</div><!-- /.slides_container -->
	<?php endif; ?>
	
	</div><!-- /#slide-box -->
	<?php if ( 'true' == $settings['slider_pagination'] ) { ?>
	<div id="line_wrap"><div id="line"></div></div>
	<?php } ?>
</div><!-- /#slides -->
