<?php

/*-----------------------------------------------------------------------------------*/
/* Start WooThemes Functions - Please refrain from editing this section */
/*-----------------------------------------------------------------------------------*/

// Set path to WooFramework and theme specific functions
$functions_path = get_template_directory() . '/functions/';
$includes_path = get_template_directory() . '/includes/';

// Define the theme-specific key to be sent to PressTrends.
define( 'WOO_PRESSTRENDS_THEMEKEY', 'l2w4inrzp90z0fhtm478djjfr8l7klho5' );

// WooFramework
require_once ($functions_path . 'admin-init.php' );			// Framework Init

// Theme specific functionality
require_once ($includes_path . 'theme-options.php' ); 			// Options panel settings and custom settings
require_once ($includes_path . 'theme-functions.php' ); 		// Custom theme functions
require_once ($includes_path . 'theme-plugins.php' );			// Theme specific plugins integrated in a theme
require_once ($includes_path . 'theme-actions.php' );			// Theme actions & user defined hooks
require_once ($includes_path . 'theme-comments.php' ); 			// Custom comments/pingback loop
require_once ($includes_path . 'theme-js.php' );				// Load javascript in wp_head
require_once ($includes_path . 'sidebar-init.php' );			// Initialize widgetized areas
require_once ($includes_path . 'theme-widgets.php' );			// Theme widgets

// Load WooCommerce functions, if applicable.
if ( is_woocommerce_activated() ) {
	locate_template( 'includes/theme-woocommerce.php', true );
}

/*-----------------------------------------------------------------------------------*/
/* You can add custom functions below */
/*-----------------------------------------------------------------------------------*/









/*-----------------------------------------------------------------------------------*/
/* Don't add any code below here or the sky will fall down */
/*-----------------------------------------------------------------------------------*/
?>